package com.droidcode.apps.testapp.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.droidcode.apps.testapp.Dummy
import com.droidcode.apps.testapp.MainActivity
import com.droidcode.apps.testapp.R
import kotlinx.android.synthetic.main.fragment_first_activity_two.*
import javax.inject.Inject
import javax.inject.Named

class FirstActivityFragmentTwo : Fragment() {

    @Inject
    @Named("global")
    lateinit var globalDummy: Dummy

    @Inject
    @Named("local")
    lateinit var localDummy: Dummy

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_first_activity_two, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        with((activity as MainActivity)) {
            scopedSubcomponent.inject(this@FirstActivityFragmentTwo)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tv_f2.text = "global scope dummy: $globalDummy, ${System.identityHashCode(globalDummy)}"
        tv2_f2.text = "local scope dummy: $localDummy, ${System.identityHashCode(localDummy)}"

        button.setOnClickListener {
            findNavController().navigate(R.id.action_firstActivityFragmentTwo_to_secondActivity)
        }
    }
}