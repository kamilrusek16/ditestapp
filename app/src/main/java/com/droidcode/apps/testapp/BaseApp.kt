package com.droidcode.apps.testapp

import android.app.Application
import com.droidcode.apps.testapp.di.DaggerAppComponent

class BaseApp : Application() {
    val appComponent = DaggerAppComponent.create()
}