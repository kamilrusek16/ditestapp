package com.droidcode.apps.testapp.di.modules

import com.droidcode.apps.testapp.Dummy
import com.droidcode.apps.testapp.di.SmallerScope
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class SmallerModule {

    @Provides
    @SmallerScope
    @Named("local")
    fun smallerDummy() = Dummy(15)
}