package com.droidcode.apps.testapp.di

import javax.inject.Scope

@Scope
@Retention(value = AnnotationRetention.RUNTIME)
annotation class SmallerScope
