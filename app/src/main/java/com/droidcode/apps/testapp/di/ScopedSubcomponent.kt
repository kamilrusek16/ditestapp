package com.droidcode.apps.testapp.di

import com.droidcode.apps.testapp.SecondActivity
import com.droidcode.apps.testapp.di.modules.SmallerModule
import com.droidcode.apps.testapp.fragment.FirstActivityFragmentOne
import com.droidcode.apps.testapp.fragment.FirstActivityFragmentTwo
import dagger.Subcomponent

@SmallerScope
@Subcomponent(modules = [SmallerModule::class])
interface ScopedSubcomponent {

    fun inject(secondActivity: SecondActivity)
    fun inject(firstActivityFragmentOne: FirstActivityFragmentOne)
    fun inject(firstActivityFragmentTwo: FirstActivityFragmentTwo)

    @Subcomponent.Factory
    interface Factory {
        fun create(): ScopedSubcomponent
    }

}