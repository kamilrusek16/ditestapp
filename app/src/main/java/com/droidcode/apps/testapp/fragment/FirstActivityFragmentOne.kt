package com.droidcode.apps.testapp.fragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.droidcode.apps.testapp.Dummy
import com.droidcode.apps.testapp.Dummy2
import com.droidcode.apps.testapp.MainActivity
import com.droidcode.apps.testapp.R
import kotlinx.android.synthetic.main.fragment_first_activity_one.*
import javax.inject.Inject
import javax.inject.Named

class FirstActivityFragmentOne : Fragment() {

    @Inject
    @Named("global")
    lateinit var globalDummy: Dummy

    @Inject
    @Named("local")
    lateinit var localDummy: Dummy

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tv_f1.text = "global scope dummy: $globalDummy, ${System.identityHashCode(globalDummy)}"
        tv2_f1.text = "local scope dummy: $localDummy, ${System.identityHashCode(localDummy)}"
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        with((activity as MainActivity)) {
            scopedSubcomponent.inject(this@FirstActivityFragmentOne)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first_activity_one, container, false)
    }
}