package com.droidcode.apps.testapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.Navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.droidcode.apps.testapp.di.AppComponent
import com.droidcode.apps.testapp.di.ScopedSubcomponent
import com.droidcode.apps.testapp.di.modules.SmallerModule
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    lateinit var appComponent: AppComponent
    lateinit var scopedSubcomponent: ScopedSubcomponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        appComponent = (applicationContext as BaseApp).appComponent
        scopedSubcomponent = appComponent.scopedSubcomponent().create()

        val navController = findNavController(this, R.id.nav_host_fragment)
        findViewById<BottomNavigationView>(R.id.bottom_navigation).setupWithNavController(navController)
    }
}