package com.droidcode.apps.testapp.di

import com.droidcode.apps.testapp.SecondActivity
import com.droidcode.apps.testapp.di.modules.AppModule
import com.droidcode.apps.testapp.fragment.FirstActivityFragmentOne
import com.droidcode.apps.testapp.fragment.FirstActivityFragmentTwo
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun scopedSubcomponent(): ScopedSubcomponent.Factory
}
