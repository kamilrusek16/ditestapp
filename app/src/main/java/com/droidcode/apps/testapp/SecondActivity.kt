package com.droidcode.apps.testapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_second.*
import kotlinx.android.synthetic.main.fragment_first_activity_two.*
import javax.inject.Inject
import javax.inject.Named

class SecondActivity : AppCompatActivity() {

    @Inject
    @Named("global")
    lateinit var globalDummy: Dummy

    @Inject
    @Named("local")
    lateinit var localDummy: Dummy

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        (applicationContext as BaseApp).appComponent.scopedSubcomponent().create().inject(this)

        tv_a2.text = "global scope dummy: $globalDummy, ${System.identityHashCode(globalDummy)}"
        tv2_a2.text = "local scope dummy: $localDummy, ${System.identityHashCode(localDummy)}"
    }
}