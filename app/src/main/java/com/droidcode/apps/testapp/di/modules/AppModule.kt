package com.droidcode.apps.testapp.di.modules

import com.droidcode.apps.testapp.Dummy
import com.droidcode.apps.testapp.di.ScopedSubcomponent
import com.droidcode.apps.testapp.di.SmallerScope
import com.droidcode.apps.testapp.fragment.FirstActivityFragmentOne
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import javax.inject.Named
import javax.inject.Singleton

@Module(subcomponents = [ScopedSubcomponent::class])
class AppModule {

    @Provides
    @Singleton
    @Named("global")
    fun provideDummy() = Dummy(1)
}